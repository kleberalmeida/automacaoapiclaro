package testServicos;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;

import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utils.RestUtils;

public class ConsultaCep {
	String url = "http://viacep.com.br/ws/";
	
	
	@Test
	public void validaStatusCode() {

		String cep = "04709111";
		String endpoint = cep.concat("/json/");
		RestUtils.setEndpoint(endpoint);
		
		RestUtils.setUrl(url);
			
		RestUtils.get();
		assertEquals(200, RestUtils.getStatusCode());
		
	}
	
	
	@Test	
	public void validaDadosCEP() {
		
		String cep = "04709111";
		String endpoint = cep.concat("/json/");
		RestUtils.setEndpoint(endpoint);
		LinkedHashMap<String, String> header = new LinkedHashMap<>();
		
		RestUtils.setUrl(url);
		
		RestUtils.get(header);
		
		
		assertEquals(200, RestUtils.getStatusCode());
		assertEquals("Rua Henri Dunant", RestUtils.getValue("logradouro"));
		assertEquals("Santo Amaro",  RestUtils.getValue("bairro"));
		assertEquals("São Paulo",  RestUtils.getValue("localidade"));
		
		
		
	}
     

}
